# frozen_string_literal: true

# Helper for page contact
module ContactHelper
  def field(input, name, text = nil, error = nil)
    capture do
      concat label_tag input, name, class: 'prefix'
      concat text_field_tag input, text, placeholder: error, class: 'form'
    end
  end

  def textarea(input, name, text = nil)
    capture do
      concat label_tag input, name, class: 'prefix'
      concat text_area_tag input, text, class: 'form'
    end
  end
end
