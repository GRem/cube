# frozen_string_literal: true

# Helper for page social
module SocialHelper
  def social(name, link)
    content_tag :div, class: 'row' do
      content_tag :div, class: 'describ' do
        concat social_logo(name)
        concat social_describ(name, link)
      end
    end
  end

  private

  def social_logo(name)
    content_tag :div, class: 'image-logo' do
      image_tag "#{name}.svg", width: 100
    end
  end

  def social_describ(name, link)
    content_tag :div, class: 'title' do
      concat social_describ_title(name, link)
      concat social_describ_text(name)
    end
  end

  def social_describ_title(name, link)
    content_tag :div do
      link_to link do
        content_tag :h3, t("welcome.faces.social.#{name}.title")
      end
    end
  end

  def social_describ_text(name)
    content_tag :div do
      t("welcome.faces.social.#{name}.describ")
    end
  end
end
