# frozen_string_literal: true

# Helper for page link
module LinkHelper
  def link_supports(name)
    content_tag :div, class: 'row' do
      concat link_logo(name)
      concat link_text(name)
    end
  end

  private

  def link_logo(name)
    content_tag :div, class: 'image-logo' do
      image_tag "#{name}.png", width: 100
    end
  end

  def link_text(name)
    content_tag :p, class: 'desc' do
      link_to t("welcome.faces.link.#{name}.link") do
        content_tag(:strong, t("welcome.faces.link.#{name}.title"))
      end
    end
  end
end
