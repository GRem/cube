# frozen_string_literal: true

# Helper for page my
module MyHelper
  def cv_link
    "cv-#{define_locale}.pdf"
  end
end
