# frozen_string_literal: true

# Helper for each component to face
module WelcomeHelper
  def fa_icon(image)
    image_tag image, size: '25x25'
  end

  def buttons_cube(direction, classe, container)
    content_tag :div, class: "buttons #{container}-button" do
      content_tag :div, class: "show-#{direction}" do
        classe.html_safe
      end
    end
  end

  def define_locale
    params.key?(:locale) ? params[:locale] : I18n.default_locale
  end

  def selector_languages_data
    [
      ['Français', :fr],
      ['English', :en]
    ]
  end
end
