# frozen_string_literal: true

# Helper for page technology
module TechnologyHelper
  def techno(image, _text)
    content_tag :div, class: 'techno' do
      techno_image(image)
    end
  end

  private

  def techno_image(image)
    content_tag :div, class: 'image' do
      image_tag image
    end
  end
end
