# frozen_string_literal: true

# Helper for page work
module WorkHelper
  def project_view(name, playstore)
    content_tag :div, class: "row #{name}" do
      concat project_view_title(name, playstore)
      concat project_view_describ(name)
    end
  end

  private

  def project_view_title(name, playstore)
    content_tag :div, class: 'title' do
      content_tag :div do
        concat project_view_title_image(name)
        concat project_view_title_name(name, playstore)
      end
    end
  end

  def project_view_title_name(name, playstore)
    content_tag :div, class: 'title' do
      concat project_view_title_text(name)
      concat project_view_title_link(playstore)
    end
  end

  def project_view_title_image(name)
    content_tag :div, class: 'image-logo' do
      image_tag "#{name}.png", width: 100
    end
  end

  def project_view_title_text(name)
    content_tag :h3 do
      link_to t("welcome.faces.work.#{name}.link") do
        t "welcome.faces.work.#{name}.title"
      end
    end
  end

  def project_view_title_link(playstore)
    content_tag :h3 do
      link_to playstore, class: 'play-store' do
        image_tag 'play-store-download.png', height: 64
      end
    end
  end

  def project_view_describ(name)
    content_tag :p, class: 'describ' do
      t "welcome.faces.work.#{name}.describ"
    end
  end
end
