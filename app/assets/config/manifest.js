// app/assets/config/manifest.js
//
// Stylesheet
//= link application.css
//
// Javascript
//= link application.js
//
// Generic
//= link language.svg
//= link logo.png
//= link pdf.svg
//= link favicon.png
//
// Faces
//= link user.svg
//= link link.svg
//= link mail.svg
//= link work.svg
//= link tech.svg
//= link social.svg
//
// Projects
//= link play-store-download.png
//= link openbd.png
//= link puzzlequest.png
//
// Socials
//= link gitlab.svg
//= link github.svg
//= link mastodon.svg
//
// Technologies
//= link aws.svg
//= link docker.svg
//= link godot.svg
//= link git.svg
//= link grafana.svg
//= link kotlin.svg
//= link mongodb.svg
//= link packer.svg
//= link python.svg
//= link ruby.svg
//= link rails.svg
//= link terraform.svg
//= link vuejs.svg
//
// Files
//= link cv-fr.pdf
//= link cv-en.pdf
//
// Links
//= link stilobique.png
//= link framasoft.png
//= link quadrature.png
//= link liberapay.png
//
// Fonts
//= link JetBrainsMono-ExtraLight.woff2
