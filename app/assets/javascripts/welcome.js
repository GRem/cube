(function() {
  var changeFace, init, khandle;

  init = function() {
    var box, i, len, onClick, panelClassName, panelClassNameActive,
        showPanelButtons;
    box = document.querySelector('.container').children[0];
    showPanelButtons = document.querySelectorAll('.show-buttons .buttons .fa');
    panelClassName = 'show-front';
    panelClassNameActive = 'active';
    onClick = function(event) {
      var faces, figure, j, str;
      j = 0;
      figure = event.target.parentNode;
      figure = figure.parentNode;
      figure = figure.parentNode;
      figure = figure.parentNode;
      figure = figure.parentNode;
      box.removeClassName(panelClassName);
      figure.classList.remove('active');
      panelClassName = event.target.parentNode.className;
      box.addClassName(panelClassName);
      str = panelClassName.split("-").pop();
      faces = document.querySelectorAll("figure." + str);
      while (j < faces.length) {
        faces[j].addClassName(panelClassNameActive);
        j++;
      }
    };
    i = 0;
    len = showPanelButtons.length;
    while (i < len) {
      showPanelButtons[i].addEventListener('click', onClick, false);
      i++;
    }
  };

  window.addEventListener('DOMContentLoaded', init, false);

  changeFace = function(e, key, place) {
    var btns, cube, keyboard, lesClass, str;
    cube = document.querySelector("#cube");
    keyboard = String.fromCharCode(e.keyCode || e.charCode);
    if (keyboard === key) {
      applyNewFace(place, cube);
    }
  };

  changeFaceClick = function(e, place) {
    var btns, cube, keyboard, lesClass, str;
    cube = document.querySelector("#cube");
    keyboard = String.fromCharCode(e.keyCode || e.charCode);

    applyNewFace(place, cube);
  };

  khandle = function(event) {
    for (let direction of [['z', 'top'], ['w', 'top'], ['s', 'bottom'], ['q', 'left'], ['a', 'left'], ['d', 'right']]) {
      changeFace(event, direction[0], direction[1]);
    }
  };

  document.onkeypress = khandle;

  for (let direction of ['right', 'left', 'top', 'bottom']) {
    collectionButton(direction)
  }

  const form = document.getElementById("form-contact");

  form.addEventListener('focusin', (event) => {
    document.onkeypress = null;
  });

  form.addEventListener('focusout', (event) => {
    document.onkeypress = khandle;
  });
}).call(this);
