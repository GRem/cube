(() => {
  Element.prototype.hasClassName = (a) => {
    return new RegExp('(?:^|\s+)' + a + '(?:\s+|$)').test(this.className);
  };

  Element.prototype.addClassName = (a) => {
    if (!this.hasClassName(a)) {
      this.className = [ this.className, a ].join(' ');
    }
  };

  Element.prototype.removeClassName = (b) => {
    var a;
    if (this.hasClassName(b)) {
      a = this.className;
      this.className =
          a.replace(new RegExp('(?:^|\s+)' + b + '(?:\s+|$)', 'g'), ' ');
    }
  };

  Element.prototype.toggleClassName = (a) => {
    this[this.hasClassName(a) ? 'removeClassName' : 'addClassName'](a);
  };

  collectionButton = (name) => {
    collection = document.getElementsByClassName(name+'-button');

    for(let button of collection) {
      button.addEventListener('click', (event) => {
        changeFaceClick(event, name);
      });
    };
  };

  applyNewFace = (place, cube) => {
    btns = document.querySelector("figure.active .buttons." + place + "-button div").className;
    lesClass = "cube-show " + btns;
    cube.className = lesClass;
    document.querySelector("figure.active").classList.remove("active");
    str = btns.split("-").pop();
    document.querySelector("figure." + str).classList.add("active");
  };
}).call(this);
