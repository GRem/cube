# frozen_string_literal: true

# Controller for faces
class WelcomeController < ApplicationController
  invisible_captcha only: [:contact], honeypot: :subtitle

  def index
    @face_show = params[:show] || 'show-front'
  end

  def contact
    respond_to do |format|
      ContactMailer.with(parameters).new_contact_email.deliver_now

      format.html { redirect_to redirected }
    end
  end

  private

  def parameters
    {
      name: params[:name],
      email: params[:email],
      subject: params[:subject],
      message: params[:message]
    }
  end

  def redirected
    {
      controller: :welcome,
      action: :index,
      show: 'show-left',
      na: params[:name],
      em: params[:email],
      su: params[:subject],
      me: params[:message]
    }
  end
end
