# frozen_string_literal: true

# Class for sending email via contact form
class ContactMailer < ActionMailer::Base
  default from: 'contact@dev-crea.com'

  def new_contact_email
    @subject = params[:subject]
    @name = params[:name]
    @email = params[:email]
    @message = params[:message]
    @to = 'vaillant.jeremy@dev-crea.com'

    mail parameters do |format|
      format.html { render layout: 'contact'  }
    end
  end

  private

  def parameters
    {
      to: @to,
      cc: @email,
      subject: 'dev-crea - Contact'
    }
  end
end
