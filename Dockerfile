FROM ruby:3-alpine

LABEL maintainer="VAILLANT Jérémy"
LABEL environment="production"
LABEL application="Dev-Crea // Cube"
LABEL description="App cube // CV"
LABEL version="1.0"

WORKDIR /app

ADD . ./

RUN apk add --update --no-cache \
    tzdata \
    nodejs \
  && apk add --virtual build-dependencies \
    make \
    g++ \
    gcc \
  && gem update --system \
  && bundle update \
  && bundle check || bundle install \
    --retry 3 \
    --without "local" \
  && rm -rf /var/cache/apk/* /usr/local/bundle/cache/*.gem \
  && apk del build-dependencies \
  && rake assets:precompile

EXPOSE 3000

CMD bundle exec rails s -p 3000 -b '0.0.0.0'
