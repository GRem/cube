# frozen_string_literal: true

require 'config'

Config.setup do |config|
  config.const_name = 'Settings'
end
