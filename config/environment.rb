# frozen_string_literal: true

# Load the Rails application.
require File.expand_path('application', __dir__)

# Initialize the Rails application.
Rails.application.initialize!

Rails.application.configure do
  # Action Mailer configuration
  config.action_mailer.smtp_settings = {
    user_name: 'contact@dev-crea.com',
    password: ENV['PASSWORD_MAIL'],
    address: 'mail.gandi.net',
    port: 587,
    authentication: :plain,
    enable_starttls_auto: true,
    domain: 'dev-crea.com'
  }
end
