# frozen_string_literal: true

require File.expand_path('boot', __dir__)

require 'rails'
require 'active_model/railtie'
require 'active_job/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Cube
  # Class override Rails::Application
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    config.time_zone = 'Paris'
    config.available_locales = %i[en fr]
    config.i18n.default_locale = :fr

    config.hosts = [
      IPAddr.new('0.0.0.0/0'),        # All IPv4 addresses.
      IPAddr.new('::/0'),             # All IPv6 addresses.
      ENV['RAILS_HOSTS']  # Additional comma-separated hosts for development.
    ]
  end
end
